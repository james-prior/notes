invocation

    invoked as an interactive login shell, or with `--login'

        execute in following order (if exists)

            /etc/profile

            first of following which is readable:
            ~/.bash_profile, ~/.bash_login or ~/.profile

            ~/.bash_logout upon logout

    invoked as an interactive non-login shell

        execute in following order (if exists)

            ~/.bashrc (often refered to by ~/.bash_profile)

    invoked non-interactively (i.e., a shell script)

        execute file defined by BASH_ENV

    invoked with sh command

        execute in following order (if exists)

            /etc/profile

            ~/.profile

    Because the only file executed by interactive non-login shells is ~/.bashrc,
    most stuff goes in ~/.bashrc.

        Or should one put stuff that can be inherited in ~/.bash_profile,
        and minimize how much is put in ~/.bashrc?

            That will probably work for environment variables,
            which can be inherited.

            Will that work for other things such as functions and aliases?

    Because interactive login shells do not (directly) call ~/.bashrc,
    ~/.bash_profile, ~/.bash_login or ~/.profile will need to source ~/.bashrc.

    Empirically, in macos 11.4, when I:
        start a new Terminal.app window or tab, or
        create a new window or pane within tmux,
    the first of the following which exists is executed:
    ~/.bash_profile, ~/.bash_login or ~/.profile.

        Those files better have something to force bash,
        otherwise the shell defaults to zsh.

    Empirically, in macos 11.4, when I:
        execute `bash` to start a sub-shell,
        execute `pipenv shell`, or
        execute `pipenv run bash`
    only ~/.bashrc is executed.

    When a bash shell script is executed,
    if the BASH_ENV shell variable is set,
    then the file defined by BASH_ENV is executed before script.

        (empirically, the file defined by BASH_ENV might be executed multiple times.
        also, it might try to execute the _output_ from that file)

    ---

    # Docker

    When _building_ Docker image based on Debian GNU/Linux 11

        and Dockerfile has SHELL ["/bin/bash", "-c"] and
        RUN command is used to run some bash script
        then none of the following files are executed even if they exist.

            ~/.bashrc
            ~/.bash_profile
            ~/.bash_login
            ~/.profile

        A workaround is to explicitly execute the startup script before
        executing the command(s) of interest. For example,

            RUN source .bashrc && ./install-stuff

    Debian GNU/Linux 11 _running_ in a Docker container

        Empirically, when I shell into a docker container running Debian
        GNU/Linux 11, with a command like

            docker exec -it <container_name> /bin/bash 

        ~/.bashrc is executed.
        ~/.bash_profile, ~/.bash_login, and ~/.profile are _not_ executed.
